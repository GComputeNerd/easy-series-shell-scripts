wd=$(pwd)
user=$(whoami)

if [ "$EUID" -ne 0 ]; then
	echo "Please run as root"
	exit 99
else
	cd ..
	mv $wd /opt/eseries
	cd /opt/eseries
	rm install.sh
	echo "Adding eseries to PATH"
	ln -s /opt/eseries/bin/* /bin
	echo "Completed!!"
fi